pragma solidity ^0.5.2;

/**
 * @title Interface contract for a compliancy chercker.
 * A compliancy checker is intended to inform about the compliancy level of a target.
 */
contract ICompliancyChecker
{
    /**
     * Get the compliancy level for target.
     * @param target address Address of the target.
     * @return compliancyLevel uint256 Compliancy level.
     */
    function getCompliancyLevelFor(address target)
        public
        view
        returns (uint256 compliancyLevel);

    /**
     * Get the name associated with a compliancy level.
     * @param compliancyLevel uint256 Level to get the name for.
     * @return compliancyLevelName string memory Name associated with the compliancy level.
     */
    function getCompliancyLevelName(uint256 compliancyLevel)
        public
        pure
        returns (string memory compliancyLevelName);
}