pragma solidity ^0.5.2;

import "../node_modules/openzeppelin-solidity/contracts/payment/escrow/Escrow.sol";

contract RefundableFromFallback
{
    // refund escrow used to hold funds while crowdsale is running
    Escrow private _escrow;

    /**
     * Event to signal a deposit via the fallback function.
     * @param sender address The depositer.
     * @param value uint256 The amount deposited.
     */
    event DepositedFromFallback(
        address indexed sender,
        uint256 value);

    /**
     * Note that other contracts will transfer funds with a base gas stipend
     * of 2300, which is not enough to call buyTokens. Consider calling
     * buyTokens directly when purchasing tokens from a contract.
     */
    function () external payable {
        // Signal.
        emit DepositedFromFallback(msg.sender, msg.value);

        // Could be not enough gas left for the following.

        // Sending fund to escrow.
        _escrow.deposit.value(msg.value)(msg.sender);
    }

    /**
     * Claim a transfer of wei sent to the fallback function.
     * Requires the deposit of refundee is more than 0.
     * Requires a validation of the refund.
     * @param refundee Whose refund will be claimed.
     * @return True on success.
     */
    function claimRefundOfSentToFallback(address payable refundee)
        public
        returns (bool succeed)
    {
        uint256 depositOfRefundee = _escrow.depositsOf(refundee);
        require(depositOfRefundee > 0,
            "RefundableFromFallback: deposit is not greater than 0.");
        require(_isRefundOfDepositOnFallbackValid(refundee, depositOfRefundee),
            "RefundableFromFallback: refund is not valid.");

        _escrow.withdraw(refundee);
        return true;
    }

    /**
     * @dev Abstract function to validate a refund. This can be used to temporary stop all refunds.
     * @param refundee address Address of the refundee.
     * @param depositOfRefundee uint256 Amount of wei deposited by the refundee.
     */
    function _isRefundOfDepositOnFallbackValid(address refundee, uint256 depositOfRefundee)
        internal
        view
        returns (bool isValid);
}