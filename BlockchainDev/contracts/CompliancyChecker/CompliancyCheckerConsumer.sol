pragma solidity ^0.5.2;

import "./ICompliancyChecker.sol";
import "../../node_modules/openzeppelin-solidity/contracts/access/roles/WhitelistedRole.sol";

/** @title Base contract for a compliancy checker consumer.
 * Designed to be inherited by a contract which needs to check compliancy information about address.
 */
contract CompliancyCheckerConsumer
    is WhitelistedRole
{
    // The underlying compliancy checker.
    ICompliancyChecker public compliancyChecker;


    // ### Events ###

    /**
     * Event to signal a change on data index for a valid token ID.
     * @param oldCompliancyChecker ICompliancyChecker Old compliancy checker.
     * @param currentCompliancyChercker ICompliancyChecker New compliancy checker.
     */
    event CompliancyCheckerChanged(
        ICompliancyChecker indexed oldCompliancyChecker,
        ICompliancyChecker indexed currentCompliancyChercker);

    /**
     * Event to signal a change on data index for a valid token ID.
     * @param oldCompliancyChecker ICompliancyChecker Old compliancy checker.
     */
    event CompliancyCheckerCleared(
        ICompliancyChecker indexed oldCompliancyChecker);


    // ### Public functions ###

    /**
     * Set the underlying compliancy checker.
     * Requires a valid address.
     * Requires to be called by a white-listed role.
     * @param newCompliancyChecker ICompliancyChecker The new compliancy checker to use.
     */
    function setCompliancyChecker(ICompliancyChecker newCompliancyChecker)
        public
        onlyWhitelisted
    {
        require(address(newCompliancyChecker) != address(0),
            "CompliancyCheckerConsumer: newCompliancyChecker is not a valid address.");

        ICompliancyChecker old = compliancyChecker;
        compliancyChecker = newCompliancyChecker;

        emit CompliancyCheckerChanged(old, compliancyChecker);
    }

    /**
     * Clear the underlying compliancy checker.
     * Requires to be called by a white-listed role.
     */
    function clearCompliancyChecker()
        public
        onlyWhitelisted
    {
        ICompliancyChecker old = compliancyChecker;

        compliancyChecker = ICompliancyChecker(address(0));

        emit CompliancyCheckerCleared(old);
    }


    // ### Modifiers ###

    /**
     * Modifier to require that target address has a minimum compliancy level.
     * Always succeed if no compliancy checker is configured.
     * @param target address Address of the target.
     * @param minLevel uint256 Minimum level to be compiant.
     */
    modifier requireCompliancyLevelAbove(address target, uint256 minLevel)
    {
        require(_isCompliancyLevelAbove(target, minLevel),
            "CompliancyCheckerConsumer: compliancy level does not satisfies requirements.");

        _;
    }

    /**
     * Tests if the target address has a minimum compliancy level.
     * Always succeed if no compliancy checker is configured.
     * @param target address Address of the target.
     * @param minLevel uint256 Minimum level to be compiant.
     * @return satisfied bool True is compliancy level of target is more than or equal to minLevel.
     */
    function _isCompliancyLevelAbove(address target, uint256 minLevel)
        internal
        view
        returns (bool satisfied)
    {
        if(address(compliancyChecker) != address(0))
        {
            uint256 compliancyLevel = compliancyChecker.getCompliancyLevelFor(target);
            return compliancyLevel >= minLevel;
        }

        return true;
    }

    /**
     * Modifier to require that target address has a maximum compliancy level.
     * Always succeed if no compliancy checker is configured.
     * @param target address Address of the target.
     * @param maxLevel uint256 Maximum level to be compiant.
     */
    modifier requireCompliancyLevelUnder(address target, uint256 maxLevel)
    {
        require(_isCompliancyLevelUnder(target, maxLevel),
            "CompliancyCheckerConsumer: compliancy level does not satisfies requirements.");

        _;
    }

    /**
     * Tests if the target address has a maximum compliancy level.
     * Always succeed if no compliancy checker is configured.
     * @param target address Address of the target.
     * @param maxLevel uint256 Maximum level to be compiant.
     * @return satisfied bool True is compliancy level of target is less than or equal to maxLevel.
     */
    function _isCompliancyLevelUnder(address target, uint256 maxLevel)
        internal
        view
        returns (bool satisfied)
    {
        if(address(compliancyChecker) != address(0))
        {
            uint256 compliancyLevel = compliancyChecker.getCompliancyLevelFor(target);
            return compliancyLevel <= maxLevel;
        }

        return true;
    }
}