pragma solidity ^0.5.2;

import "../node_modules/openzeppelin-solidity/contracts/utils/ReentrancyGuard.sol";
import "../node_modules/openzeppelin-solidity/contracts/access/roles/WhitelistedRole.sol";
import "../node_modules/openzeppelin-solidity/contracts/access/roles/PauserRole.sol";
import "../contracts/VoxelToken.sol";
import "../contracts/CompliancyChecker/CompliancyCheckerConsumer.sol";
import "../contracts/TokenCostComputer/ITokenCostComputer.sol";
import "../contracts/TokenCostComputer/TokenCostComputerConsumer.sol";

contract VoxelTokenCrowdsale is
    ReentrancyGuard,
    WhitelistedRole,
    Pausable,
    CompliancyCheckerConsumer,
    TokenCostComputerConsumer
{
    using SafeMath for uint256;

    // VoxelToken target of this crowdsale
    VoxelToken public voxelToken;

    // Address where funds are collected
    address payable public wallet;

    // Amount of wei raised
    uint256 public weiRaised;


    // ### Events ###

    /**
     * Event for token purchase logging
     * @param purchaser who paid for the tokens
     * @param beneficiary who got the tokens
     * @param value weis paid for purchase
     * @param amount amount of tokens purchased
     */
    event TokensPurchased(
        address indexed purchaser,
        address indexed beneficiary,
        uint256 value,
        uint256 amount);


    // ### Constructor ###

    /** Contructor.
     * @param token_ VoxelToken Token to sale.
     * @param wallet_ address payable Address of the wallet to receive the sale payments.
     * @param tokenCostComputer_ ITokenCostComputer Token cost computer.
     */
    constructor(
        VoxelToken token_,
        address payable wallet_,
        ITokenCostComputer tokenCostComputer_)
        public
        TokenCostComputerConsumer(tokenCostComputer_)
    {
        require(address(token_) != address(0),
            "VoxelTokenCrowdsale: token address is not valid.");
        require(wallet_ != address(0),
            "VoxelTokenCrowdsale: wallet address is not valid.");

        voxelToken = token_;
        wallet = wallet_;
    }


    // ### Buying Functions ###

    /**
     * Buy a token.
     * This function has a non-reentrancy guard, so it shouldn't be called by
     * another `nonReentrant` function.
     * @param beneficiary Recipient of the token purchase.
     * @param tokenId uint256 ID of the token to claim.
     * @param tokenDataIndex uint8 Index of the data of the token.
     */
    function buyToken(
        address beneficiary,
        uint256 tokenId,
        uint8 tokenDataIndex)
        public
        nonReentrant
        whenNotPaused
        payable
    {
        // msg.value validation.
        require(msg.value != 0,
            "VoxelTokenCrowdsale: msg.value must be greater than 0.");

        // beneficiary validation.
        require(beneficiary != address(0),
            "VoxelTokenCrowdsale: beneficiary is not a valid address.");

        // Compliancy could be AML, KYC, etc.
        require(_isCompliancyLevelAbove(beneficiary, 1),
            "VoxelTokenCrowdsale: beneficiary compliancy does not satisfy requirements.");

        // Cost validation.
        require(msg.value >= getTokenCost(beneficiary, tokenId, tokenDataIndex),
            "VoxelTokenCrowdsale: msg.value is not enough to buy a token.");

        // Update state.
        weiRaised = weiRaised.add(msg.value);

        voxelToken.mint(beneficiary, tokenId, tokenDataIndex);
        emit TokensPurchased(msg.sender, beneficiary, msg.value, tokenId);

        _forwardFunds();
    }

    /**
     * Buy a token.
     * This function has a non-reentrancy guard, so it shouldn't be called by
     * another `nonReentrant` function.
     * @param beneficiary Recipient of the token purchase.
     * @param tokensId uint256[] ID of the token to claim.
     * @param tokenDataIndices uint8[] Index of the data of the token.
     */
    function batchBuyToken(
        address beneficiary,
        uint256[] memory tokensId,
        uint8[] memory tokenDataIndices)
        public
        nonReentrant
        whenNotPaused
        payable
    {
        // msg.value validation.
        require(msg.value != 0,
            "VoxelTokenCrowdsale: msg.value must be greater than 0.");

        // beneficiary validation.
        require(beneficiary != address(0),
            "VoxelTokenCrowdsale: beneficiary is not a valid address.");

        // Compliancy could be AML, KYC, etc.
        require(_isCompliancyLevelAbove(beneficiary, 1),
            "VoxelTokenCrowdsale: beneficiary compliancy does not satisfy requirements.");

        // Total tokens cost initialization. Used to check token cost requirements.
        uint256 totalTokensCost = 0;

        // Loops over all inputs.
        for(uint256 i = 0; i < tokensId.length; ++i)
        {
            uint256 tokenId = tokensId[i];
            uint8 tokenDataIndex = tokenDataIndices[i];
            uint256 tokenCost_ = getTokenCost(beneficiary, tokenId, tokenDataIndex);
            totalTokensCost += tokenCost_;  // No need to waste wei in safe arithmetic operation.

            voxelToken.mint(beneficiary, tokenId, tokenDataIndex);
            emit TokensPurchased(msg.sender, beneficiary, tokenCost_, tokenId);
        }

        // Cost validation.
        require(msg.value >= totalTokensCost,
            "VoxelTokenCrowdsale: msg.value is not enough to buy a token.");

        // Update state. ! State is updated after interactions !
        weiRaised = weiRaised.add(msg.value);

        // Forward funds.
        _forwardFunds();
    }


    // ### Cost computing Functions ###

    /**
     * @dev Get cost in wei for buying a token.
     * @param beneficiary Recipient of the token purchase.
     * @param tokenId uint256 ID of the token to claim.
     * @param tokenDataIndex uint8 Index of the data of the token.
     */
    function getTokenCost(
        address beneficiary,
        uint256 tokenId,
        uint8 tokenDataIndex)
        public
        view
        returns (uint256 weiCost)
    {
        return tokenCostComputer.getTokenCost(
            tokenId,
            beneficiary,
            toBytesEth(tokenDataIndex));
    }


    // ### Forwarding Functions ###

    /**
     * @dev Determines how ETH is stored/forwarded on purchases.
     */
    function _forwardFunds() internal {
        wallet.transfer(msg.value);
    }


    // ### Fallback function ###

    /**
     * Fallback function. Accepts everything and log using event.
     */
    function()
        external
        payable
    {
        revert("VoxelTokenCrowdsale: No ether accepted directly. Use buyToken or batchBuyToken instead.");
    }
}