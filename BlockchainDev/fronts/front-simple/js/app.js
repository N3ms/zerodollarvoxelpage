
App = {
  // WEB3 provider.
  web3Provider: null,
  // List of contracts.
  contracts: {},
  // Instance of VoxelToken.
  voxelTokenInstance: null,
  // Instance of VoxelTokenCrowdsale.
  voxelTokenCrowdsaleInstance: null,
  // Bake of DOM elements.
  dom: {},


  init: async function() {
    // Load things and stuffs.
    dom = new Array();
    dom.voxelListTBody = $('#voxelTable > tbody');
    dom.voxelTable = $('#voxelTable > tbody:last-child');
    dom.voxelTemplate = $('#voxelTemplate');
    dom.buyVoxelForm = $('#form-buyVoxel');

    return await App.initWeb3();
  },

  initWeb3: async function() {
    console.log("Init Web3");

    // Modern dapp browsers...
    if (window.ethereum) {
      console.log("Found via window.ethereum.");
      App.web3Provider = window.ethereum;

      try {
        // Request account access
        console.log("Request account access...");
        await window.ethereum.enable();
        console.log("Access granted");
      } catch (error) {
        // User denied account access...
        console.error("User denied account access")
      }
    }
    // Legacy dapp browsers...
    else if (window.web3) {
      console.log("Found via window.web3.");
      App.web3Provider = window.web3.currentProvider;
    }
    // If no injected web3 instance is detected, fall back to Ganache
    else {
      console.log("No injected web3 instance is detected, fall back to http://localhost:7545")
      App.web3Provider = new Web3.providers.HttpProvider('http://localhost:7545');
    }
    web3 = new Web3(App.web3Provider);

    web3.eth.getAccounts(function(error, accounts) {
      if (error) {
        console.log(error);
      }

      console.log(accounts.length + " account(s).");
    });

    return App.initContract();
  },

  initContract: function() {
    $.getJSON('VoxelTokenCrowdsale.json', function(data) {
      // Get the necessary contract artifact file and instantiate it with truffle-contract
      var VoxelTokenCrowdsaleArtifact = data;
      App.contracts.VoxelTokenCrowdsale = TruffleContract(VoxelTokenCrowdsaleArtifact);
    
      // Set the provider for our contract
      App.contracts.VoxelTokenCrowdsale.setProvider(App.web3Provider);
      
      // Look up for deployed instance
      App.contracts.VoxelTokenCrowdsale.deployed()
        .then(function(instance) {
          App.voxelTokenCrowdsaleInstance = instance;
          
          console.log("Ready! VoxelTokenCrowdsale @ " + App.voxelTokenCrowdsaleInstance.address);
        })
        .catch(function(err) {
          console.log(err.message);
        });
    });

    $.getJSON('VoxelToken.json', function(data) {
      // Get the necessary contract artifact file and instantiate it with truffle-contract
      var VoxelTokenArtifact = data;
      App.contracts.VoxelToken = TruffleContract(VoxelTokenArtifact);
    
      // Set the provider for our contract
      App.contracts.VoxelToken.setProvider(App.web3Provider);
      
      // Look up for deployed instance
      App.contracts.VoxelToken.deployed()
        .then(function(instance) {
          App.voxelTokenInstance = instance;
          
          console.log("Ready! VoxelToken @ " + App.voxelTokenInstance.address);
        })
        .catch(function(err) {
          console.log(err.message);
        });
    });

    return App.bindEvents();
  },

  bindEvents: function() {
    $(document).on('click', '#btn-refreshVoxelList', App.handleRefreshVoxelList);
    $(document).on('click', '.voxel-btn-setTokenDataIndex', App.handleSetTokenDataIndex);
    dom.buyVoxelForm.on('click', "#btn-buyVoxel", App.handleBuyVoxel);
  },

  handleSetTokenDataIndex: function(event) {
    event.preventDefault();

    var eventTarget = $(event.currentTarget);
    var voxelId = parseInt(eventTarget.data('id'));
    var newVoxelDataIndex = parseInt(eventTarget.siblings('.voxel-input-dataIndex')[0].value);

    App.setTokenDataIndex(voxelId, newVoxelDataIndex);
  },

  setTokenDataIndex: function(voxelId, newVoxelDataIndex) {
    console.log("setTokenDataIndex: " + voxelId + " = " + newVoxelDataIndex);

    App.voxelTokenInstance.setTokenDataIndex(voxelId, newVoxelDataIndex)
      .then(function(error, result) {
        console.log(error, result);
      })
  },
  
  handleRefreshVoxelList: function(event) {
    event.preventDefault();

    App.refreshVoxelList();
  },

  refreshVoxelList: function(event) {
    App.clearVoxelList();

    // Brute force load.
    App.voxelTokenInstance.totalSupply()
      .then(function(totalSupply) {
        var index = 0;
        for(index = 0; index < totalSupply; ++index) {
          App.voxelTokenInstance.tokenByIndex(index)
            .then(function(tokenId) {
              App.voxelTokenInstance.ownerOf(tokenId)
                .then(function(owner) {
                  App.voxelTokenInstance.tokenDataIndex(tokenId)
                    .then(function(dataIndex) {
                      App.appendVoxelDataToVoxelList(tokenId, dataIndex, owner);
                    })
                })
            });
        }
      });

    // // Alternate strategy using events.
    // var events = App.voxelTokenInstance.allEvents({fromBlock: 0, toBlock: 'latest'});
    // events.get(function(error, log) {
    //   event_data = log;
    //   console.log(event_data);
    // });
  },

  clearVoxelList: function() {
    dom.voxelListTBody.empty();
  },

  appendVoxelDataToVoxelList: function(id, dataIndex, owner) {
    dom.voxelTemplate.find('.voxel-id').text(id);
    dom.voxelTemplate.find('.voxel-pos').text(App.voxelIdToPosition(id).join(", "));
    dom.voxelTemplate.find('.voxel-input-dataIndex').attr('value', dataIndex);
    dom.voxelTemplate.find('.voxel-btn-setTokenDataIndex').attr('data-id', id);
    dom.voxelTemplate.find('.voxel-owner').text(owner);

    dom.voxelTable.append(
      $('<tr>').append(dom.voxelTemplate.html())
    );
  },

  handleBuyVoxel: function(event) {
    event.preventDefault();

    const voxelId = dom.buyVoxelForm.find("#buy-input-id")[0].value;
    const dataIndex = dom.buyVoxelForm.find("#buy-input-dataIndex")[0].value;
    App.buyVoxel(voxelId, dataIndex);
  },

  buyVoxel: function(voxelId, dataIndex) {
    App.voxelTokenCrowdsaleInstance.getTokenCost(
      web3.eth.coinbase,
      voxelId,
      dataIndex)
      .then(function (cost) {
        const msg = `Buy Voxel:
        ${voxelId} - ${dataIndex} - ${cost} wei
        ${web3.eth.coinbase}`;
        
        console.log(msg);

        App.voxelTokenCrowdsaleInstance.batchBuyToken(
          web3.eth.coinbase,
          [voxelId],
          [dataIndex],
          {from: web3.eth.coinbase, value: cost})
          .then(function(error, result) {
            console.log(error, result);
            // App.appendVoxelDataToVoxelList(result.log)
          })
      })
  },

  handleConvertPositionToId: function() {
    const x = parseInt(dom.buyVoxelForm.find("#buy-input-x")[0].value);
    const y = parseInt(dom.buyVoxelForm.find("#buy-input-y")[0].value);
    const z = parseInt(dom.buyVoxelForm.find("#buy-input-z")[0].value);

    const voxelId = App.positionToVoxelId(x, y, z);

    dom.buyVoxelForm.find("#buy-input-id")[0].value = voxelId.toString();
  },

  positionToVoxelId: function(x, y, z) {
    const voxelIdBinary = `${(x).toString(2).padStart(16, "0")}${(y).toString(2).padStart(16, "0")}${(z).toString(2).padStart(16, "0")}`;
    return new web3.BigNumber(voxelIdBinary, 2);
  },

  voxelIdToPosition: function(voxelId) {
    const voxelIdBinary = voxelId.toString(2).padStart(48, "0");

    const x = parseInt(voxelIdBinary.substr(0, 16), 2);
    const y = parseInt(voxelIdBinary.substr(16, 16), 2);
    const z = parseInt(voxelIdBinary.substr(32, 16), 2);

    return [x, y, z];
  },


  loadFaceData: function(faceId) {
    return new Promise(function(resolve, reject) {
      var request = new XMLHttpRequest();
      request.open("GET", `./data_${faceId}.bin`, true);
      request.responseType = "arraybuffer";

      request.onload = function () {
        if (request.status === 200) {
          var arrayBuffer = request.response; // Note: not request.responseText
          if (arrayBuffer) {
            var byteArray = new Uint32Array(arrayBuffer);
            resolve(byteArray);
          }
          else {
            // If it fails, reject the promise with a error message
            reject(Error('Response received but cannot read layer data'));
          }
        }
        else {
          // If it fails, reject the promise with a error message
          reject(Error('Layer data didn\'t load successfully; error code:' + request.statusText));
        }
      };

      request.onerror = function() {
      // Also deal with the case when the entire request fails to begin with
      // This is probably a network error, so reject the promise with an appropriate message
          reject(Error('There was a network error.'));
      };

      // Send the request
      request.send();
    });
  },

  loadAllFacesData: function() {
    return new Promise(function(resolve, reject) {
      const tasks = [
        // xpos
        App.loadFaceData("xpos"),
        // xneg
        App.loadFaceData("xneg"),
        // ypos
        App.loadFaceData("ypos"),
        // yneg
        App.loadFaceData("yneg"),
        // zpos
        App.loadFaceData("zpos"),
        // zneg
        App.loadFaceData("zneg"),
      ];

      return tasks.reduce((promiseChain, currentTask) => {
          return promiseChain.then(chainResults =>
              currentTask.then(currentResult =>
                  [ ...chainResults, currentResult ]
              )
          );
      },
      Promise.resolve([]))
      .then(faceDataArrays => {
          // Do something with all results
          resolve(faceDataArrays);
      })
      .catch(() => reject(Error('There was an error during layer data aggregation.')));
    });
  },


  loadFaceAndDisplayVoxels: function() {
    App.loadAllFacesData()
    .then(/*Uint32Array[]*/ faceDataArrays => {
      faceDataArrays.forEach(/*Uint32Array*/ faceDataArray => {
        console.log(faceDataArray.length);
      });
    })
  }
};

$(function() {
  $(window).load(function() {
    App.init();
  });
});
