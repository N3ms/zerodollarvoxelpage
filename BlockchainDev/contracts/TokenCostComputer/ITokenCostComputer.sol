pragma solidity ^0.5.2;

/**
 * @title Interface contract for a token cost computer.
 * A token cost computer is intended to compute the cost of a token.
 */
interface ITokenCostComputer
{
    /**
     * Get the cost of a token.
     * @param tokenId uint256 ID of the token to compute the cost.
     * @param recipient address Optional address of the recipient.
     * @param data bytes Optional data to compute the cost.
     * @return tokenCost uint256 Cost of the token.
     */
    function getTokenCost(
        uint256 tokenId,
        address recipient,
        bytes calldata data)
        external
        view
        returns (uint256 tokenCost);
}