pragma solidity ^0.5.2;

import "./ITokenCostComputer.sol";
import "../../node_modules/openzeppelin-solidity/contracts/access/roles/WhitelistedRole.sol";

/** @title Base contract for a token cost computer.
 * Designed to be inherited by a contract which needs to compute cost of a token.
 */
contract TokenCostComputerConsumer
    is WhitelistedRole
{
    // The underlying token cost computer.
    ITokenCostComputer public tokenCostComputer;


    // ### Events ###

    /**
     * Event to signal a change on the token cost computer.
     * @param oldTokenCostComputer ITokenCostComputer Old token cost computer.
     * @param currentTokenCostComputer ITokenCostComputer New token cost computer.
     */
    event TokenCostComputerChanged(
        ITokenCostComputer indexed oldTokenCostComputer,
        ITokenCostComputer indexed currentTokenCostComputer);


    // ### Constructor ###
    /**
     * Constructor.
     * @param tokenCostComputer_ ITokenCostComputer Token cost computer.
     */
    constructor(ITokenCostComputer tokenCostComputer_)
        public
    {
        tokenCostComputer = tokenCostComputer_;
    }


    // ### Public functions ###

    /**
     * Set the underlying token cost computer.
     * Requires a valid address.
     * Requires to be called by a white-listed role.
     * @param newTokenCostComputer ITokenCostComputer The new token cost computer.
     */
    function setTokenCostComputer(ITokenCostComputer newTokenCostComputer)
        public
        onlyWhitelisted
    {
        require(address(newTokenCostComputer) != address(0),
            "TokenCostComputerConsumer: newTokenCostComputer is not a valid address.");

        ITokenCostComputer old = tokenCostComputer;
        tokenCostComputer = newTokenCostComputer;

        emit TokenCostComputerChanged(old, tokenCostComputer);
    }


    // ### Pure helper functions ###

    /**
     * Convert a uint256 to a bytes.
     * @param x uint256 Value to convert.
     * @return b bytes memory The resulting conversion.
     */
    function toBytesEth(uint256 x)
    internal
    pure
    returns (bytes memory b)
    {
        b = new bytes(32);
        for (uint i = 0; i < 32; i++) {
            b[i] = byte(uint8(x / (2**(8*(31 - i)))));
        }
    }
}