pragma solidity ^0.5.2;

import "./ITokenCostComputer.sol";
import "../../node_modules/openzeppelin-solidity/contracts/math/SafeMath.sol";
import "../../node_modules/openzeppelin-solidity/contracts/token/ERC721/IERC721Enumerable.sol";
import "../VoxelToken.sol";

/** @title Erc721TokenScarcityCostComputer.
 * A token cost computer based of ERC721 token scarcity.
 */
contract Erc721TokenScarcityCostComputer
    is ITokenCostComputer
{
    using SafeMath for uint256;

    // Min cost.
    uint256 public minCost;
    // Max cost.
    uint256 public maxCost;
    // Max token limit.
    uint256 public maxTokenLimit;
    // Cost range.
    uint256 private _costRange;

    /**
     * Constructor.
     * @param minCost_ uint256 Min cost for outer bound.
     * @param maxCost_ uint256 Max cost for center.
     * @param maxTokenLimit_ uint256 Max token limit.
     */
    constructor(
        uint256 minCost_,
        uint256 maxCost_,
        uint256 maxTokenLimit_)
        public
    {
        minCost = minCost_;
        maxCost = maxCost_;
        maxTokenLimit = maxTokenLimit_;
        _costRange = maxCost - minCost;
    }

    /**
     * Get the cost of a token.
     * @param tokenId uint256 ID of the token to compute the cost.
     * @param recipient address Optional address of the recipient.
     * @param data bytes Optional data to compute the cost.
     * @return tokenCost uint256 Cost of the token.
     */
    function getTokenCost(
        uint256 tokenId,
        address recipient,
        bytes calldata data)
        external
        view
        returns (uint256 tokenCost)
    {
        IERC721Enumerable token = IERC721Enumerable(msg.sender);
        uint256 tokenTotalSupply = token.totalSupply();

        if(tokenTotalSupply >= maxTokenLimit)
        {
            return maxCost;
        }

        // No need for SafeMath here, CostComputer is supposed to be used internally by a token.
        uint256 ratioToLimit = tokenTotalSupply.div(maxTokenLimit);
        tokenCost = minCost + ratioToLimit * _costRange;
        return tokenCost;
    }
}