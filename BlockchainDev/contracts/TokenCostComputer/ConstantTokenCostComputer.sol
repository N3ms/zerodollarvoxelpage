pragma solidity ^0.5.2;

import "./ITokenCostComputer.sol";

/** @title ConstantTokenCostComputer.
 * A token cost computer using a constant as token cost.
 */
contract ConstantTokenCostComputer
    is ITokenCostComputer
{
    // Constant cost of a token.
    uint256 public constantCost;

    constructor(uint256 constantCost_)
        public
    {
        constantCost = constantCost_;
    }

    /**
     * Get the cost of a token.
     * @param tokenId uint256 ID of the token to compute the cost.
     * @param recipient address Optional address of the recipient.
     * @param data bytes Optional data to compute the cost.
     * @return tokenCost uint256 Cost of the token.
     */
    function getTokenCost(
        uint256 tokenId,
        address recipient,
        bytes calldata data)
        external
        view
        returns (uint256 tokenCost)
    {
        return constantCost;
    }
}