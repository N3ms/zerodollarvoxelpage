var VoxelToken = artifacts.require("VoxelToken");
var VoxelTokenCrowdsale = artifacts.require("VoxelTokenCrowdsale");
var ConstantTokenCostComputer = artifacts.require("ConstantTokenCostComputer");

module.exports = function(deployer, network, accounts) {
    var voxelTokenInstance;
    var voxelTokenCrowdsaleInstance;
    var constantTokenCostComputer;

    // Deploy VoxelToken
    deployer.deploy(
        VoxelToken,
        "VoxelToken",
        "ZVOX",
        "zvoxMetadata?id=",
        "512",
        "33")
    
    .then(function(instance) {
        voxelTokenInstance = instance;

        return deployer.deploy(
            ConstantTokenCostComputer,
            // 1 Finney or Miliether.
            "1000000000000000");
    })

    // Deploy VoxelTokenCrowdsale, using VoxelToken instance's address.
    .then(function(instance) {
        constantTokenCostComputer = instance;

        return deployer.deploy(
            VoxelTokenCrowdsale,
            voxelTokenInstance.address,
            // Ganache account #1
            accounts[0],
            constantTokenCostComputer.address);
    })

    // Set VoxelTokenCrowdsale instance's permissions.
    .then(function(instance) {
        voxelTokenCrowdsaleInstance = instance;

        voxelTokenInstance.addWhitelisted(voxelTokenCrowdsaleInstance.address);
    });
};