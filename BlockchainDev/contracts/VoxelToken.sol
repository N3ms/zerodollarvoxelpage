pragma solidity ^0.5.2;

import "../node_modules/openzeppelin-solidity/contracts/access/roles/WhitelistedRole.sol";
import "../node_modules/openzeppelin-solidity/contracts/token/ERC721/ERC721Full.sol";
import "../node_modules/openzeppelin-solidity/contracts/token/ERC721/ERC721Pausable.sol";
import "../contracts/CompliancyChecker/CompliancyCheckerConsumer.sol";

contract VoxelToken
    is  ERC721Full,
        ERC721Pausable,
        WhitelistedRole,
        CompliancyCheckerConsumer
{
    // ### State variables ###

    // Base URI to save URI storage and management costs.
    bytes private _uriBase;

    // Volume characteristics: side length in number of voxel.
    uint16 public edgeVoxelLength;
    // Volume characteristics: voxel gap on edges, space with no valid (visible) voxels.
    uint16 public faceGapVoxelLegth;
    // Max tokenId based on volume characteristics.
    uint256 public maxTokenId;
    // Useful voxel count considering edgeVoxelLength and faceGapVoxelLegth.
    uint256 public usefulVoxelCount;

    // Mapping from token ID to data index.
    mapping (uint256 => uint8) private _tokenIdToDataIndex;


    // ### Events ###

    /**
     * Event to signal a change on data index for a valid token ID.
     * @param tokenId uint256 ID of the token.
     * @param newDataIndex uint8 New index of the data.
     * @param oldDataIndex uint8 Old index of the data.
     */
    event TokenDataIndexChanged(
        uint256 indexed tokenId,
        uint8 indexed newDataIndex,
        uint8 indexed oldDataIndex);

    /**
     * Event to signal a deposit via the fallback function.
     * @param sender address The depositer.
     * @param value uint256 The amount deposited.
     */
    event DepositFromFallback(
        address indexed sender,
        uint256 value);


    // ### Constructor ###
    /**
     * Constructor.
     * @param name string memory Name of the token.
     * @param symbol string memory Symbol of the token.
     * @param uriBase string memory Base of the token URI.
     * @param edgeVoxelLength_ uint16 Voxel count per edge.
     * @param faceGapVoxelLegth_ uint16 Voxel count of the no-voxel gap on each face.
     */
    constructor(
        string memory name,
        string memory symbol,
        string memory uriBase,
        uint16 edgeVoxelLength_,
        uint16 faceGapVoxelLegth_)
        public
        ERC721Full(name, symbol)
    {
        // Check that volume characteristics are consistent.
        require(edgeVoxelLength_ != 0,
            "VoxelToken: volumeSideLenght must be more than 0."); // Length is more than 0.
        _uriBase = bytes(uriBase);
        edgeVoxelLength = edgeVoxelLength_;
        faceGapVoxelLegth = faceGapVoxelLegth_;

        // Compute maxTokenId based on volume characteristics for subsequent validity checks.
        maxTokenId = uint256(edgeVoxelLength); // Initialise to max possible value.
        maxTokenId = (maxTokenId << 32 | maxTokenId << 16 | maxTokenId);    // Compute ID using bit-shifting.

        // Compute usefulVoxelCount based on volume characteristics.
        // c: edgeVoxelLength; d: faceGapVoxelLegth aka depth; n: dimension = 3(D)
        // 2 * n * d * (c - 2 * d)^(n - 1)
        usefulVoxelCount = 2 * 3 * faceGapVoxelLegth * (edgeVoxelLength - 2 * faceGapVoxelLegth) ** 2;
    }


    // ### Modifiers ###
    /**
     * @dev Modifier to require that token ID exists.
     * @param tokenId uint256 ID of the token to check.
     */
    modifier requireTokenExists(uint256 tokenId)
    {
        require(_exists(tokenId),
            "VoxelToken: token does not exist for this tokenId.");
        _;
    }


    // ### Transfer Functions ###

    /**
     * Transfers the ownership of a given token ID to another address.
     * Usage of this method is discouraged, use `safeTransferFrom` whenever possible.
     * Requires the msg.sender to be the owner, approved, or operator.
     * Requires the msg.sender to have a compliancy level above 1.
     * Requires the receiver 'to' to have a compliancy level above 1.
     * @param from address of current owner of the token.
     * @param to address to receive the ownership of the given token ID.
     * @param tokenId uint256 ID of the token to be transferred.
     */
    function transferFrom(
        address from,
        address to,
        uint256 tokenId)
        public
        whenNotPaused
        requireCompliancyLevelAbove(msg.sender, 1)
        requireCompliancyLevelAbove(to, 1)
    {
        require(_isApprovedOrOwner(msg.sender, tokenId),
            "VoxelToken: msg.sender is not approved or owner of the token.");

        super.transferFrom(from, to, tokenId);
    }


    // ### Mint Functions ###

    /**
     * Mint a batch of token for recipient.
     * Throws if the tokens ID already exists.
     * @param recipient address of the recipient.
     * @param tokensId uint256[] ID of the tokens to claim.
     * @param tokenDataIndices uint32 Indices of the data of the tokens.
     * @return True if succeed.
     */
    function batchMint(
        address recipient,
        uint256[] memory tokensId,
        uint8[] memory tokenDataIndices)
        public
        whenNotPaused
        onlyWhitelisted
        requireCompliancyLevelAbove(recipient, 1)
        returns (bool success)
    {
        require(tokensId.length == tokenDataIndices.length,
            "VoxelToken: mismatch lengths between tokensId and tokenDataIndices.");

        for(uint256 i = 0; i < tokensId.length; ++i)
        {
            require(_createVoxel(
                recipient,
                tokensId[i],
                tokenDataIndices[i]),
                "VoxelToken: one of the operation is the batch has failed.");
        }

        return true;
    }

    /**
     * Mint a token with a specific token ID and data index for recipient.
     * Throws if the token ID already exists.
     * @param recipient address of the recipient.
     * @param tokenId uint256 ID of the token to claim.
     * @param tokenDataIndex uint32 Index of the data of the token.
     * @return True if succeed.
     */
    function mint(
        address recipient,
        uint256 tokenId,
        uint8 tokenDataIndex)
        public
        whenNotPaused
        onlyWhitelisted
        requireCompliancyLevelAbove(recipient, 1)
        returns (bool success)
    {
        return _createVoxel(
            recipient,
            tokenId,
            tokenDataIndex);
    }

    /**
     * Mint a token with a specific token ID and data index for recipient.
     * Throws if the token ID already exists.
     * @param recipient address of the recipient.
     * @param tokenId uint256 ID of the token to claim.
     * @param tokenDataIndex uint32 Index of the data of the token.
     * @return True if succeed.
     */
    function _createVoxel(
        address recipient,
        uint256 tokenId,
        uint8 tokenDataIndex)
        private
        returns (bool success)
    {
        // Check that tokenId is within the accepted range.
        require(tokenId <= maxTokenId,
            "VoxelToken: token ID is out of range. Check maxTokenId.");

        // Check that tokenId is not already minted.
        require(_exists(tokenId) == false,
            "VoxelToken: cannot create token because it already exist.");

        // Mint the token.
        _mint(recipient, tokenId);

        // Complete minting by assigning data index.
        _tokenIdToDataIndex[tokenId] = tokenDataIndex;

        return true;
    }


    // ### Set token data index Functions ###

    /**
     * Set a new data index for the token with a specific token ID.
     * Requires tokenId exists.
     * Requires the msg.sender to be the owner, approved, or operator.
     * Requires that new data index is different than current data index.
     * @param tokensId uint256[] ID of the tokens to claim.
     * @param newTokenDataIndices uint32 Indices of the data of the tokens.
     * @return True if succeed.
     */
    function batchSetTokenDataIndex(
        uint256[] memory tokensId,
        uint8[] memory newTokenDataIndices)
        public
        whenNotPaused
        returns (bool succeeded)
    {
        require(tokensId.length == newTokenDataIndices.length,
            "VoxelToken: mismatch lengths between tokensId and newTokenDataIndices.");

        for(uint256 i = 0; i < tokensId.length; ++i)
        {
            _setTokenDataIndex(tokensId[i], newTokenDataIndices[i]);
        }

        return true;
    }

    /**
     * Set a new data index for the token with a specific token ID.
     * Requires tokenId exists.
     * Requires the msg.sender to be the owner, approved, or operator.
     * Requires that new data index is different than current data index.
     * @param tokenId uint256 ID of the token to claim.
     * @param newDataIndex uint32 Index of the data of the token.
     * @return True if succeed.
     */
    function setTokenDataIndex(
        uint256 tokenId,
        uint8 newDataIndex)
        public
        whenNotPaused
        returns (uint8 oldDataIndex)
    {
        return _setTokenDataIndex(tokenId, newDataIndex);
    }

    /**
     * Set a new data index for the token with a specific token ID.
     * Requires tokenId exists.
     * Requires the msg.sender to be the owner, approved, or operator.
     * Requires that new data index is different than current data index.
     * @param tokenId uint256 ID of the token to claim.
     * @param newDataIndex uint32 Index of the data of the token.
     * @return True if succeed.
     */
    function _setTokenDataIndex(
        uint256 tokenId,
        uint8 newDataIndex)
        internal
        requireTokenExists(tokenId)
        returns (uint8 oldDataIndex)
    {
        // Check that newDataIndex is different than the current value to avoid wasting gas.
        require(_tokenIdToDataIndex[tokenId] != newDataIndex,
            "VoxelToken: the new data index must be different than the current value.");

        // Check that sender is approved or owner to alter token data index.
        require(_isApprovedOrOwner(msg.sender, tokenId),
            "VoxelToken: msg.sender is not approved nor owner of the token.");

        oldDataIndex = _tokenIdToDataIndex[tokenId];

        // Assigning data index.
        _tokenIdToDataIndex[tokenId] = newDataIndex;

        // Signal
        emit TokenDataIndexChanged(tokenId, newDataIndex, oldDataIndex);

        return oldDataIndex;
    }


    // ### View functions ###

    /**
     * Returns the data index for a given token ID.
     * Throws if the token ID does not exist.
     * @param tokenId uint256 ID of the token to query.
     * @return The data index.
     */
    function tokenDataIndex(uint256 tokenId)
        external
        view
        requireTokenExists(tokenId)
        returns (uint8)
    {
        return _tokenIdToDataIndex[tokenId];
    }

    /**
     * Returns an URI for a given token ID.
     * Throws if the token ID does not exist. May return an empty string.
     * @param tokenId uint256 ID of the token to query.
     * @dev Idea from https://medium.com/coinmonks/jumping-into-solidity-the-erc721-standard-part-6-7ea4af3366fd.
     * @return The corresponding URI.
     */
    function tokenURI(uint256 tokenId)
        external
        view
        requireTokenExists(tokenId)
        returns (string memory)
    {
        // Prepare our tokenId's byte array.
        uint maxLength = 78;
        bytes memory reversed = new bytes(maxLength);
        uint i = 0;
        uint256 tmpTokenId = tokenId;

        // Loop through and add byte values to the array.
        while (tmpTokenId != 0) {
            uint8 remainder = uint8(tmpTokenId % 10);
            tmpTokenId /= 10;
            reversed[i++] = byte(48 + remainder);
        }

        // Prepare the final array.
        bytes memory s = new bytes(_uriBase.length + i);
        uint j;

        // Add the base to the final array.
        for (j = 0; j < _uriBase.length; j++) {
            s[j] = _uriBase[j];
        }

        // Add the tokenId to the final array.
        for (j = 0; j < i; j++) {
            s[j + _uriBase.length] = reversed[i - 1 - j];
        }

        // Turn it into a string and return it.
        return string(s);
    }


    // ### Pure functions ###

    /**
     * Returns the (x, y, z) position for a given token ID.
     * @param tokenId uint256 ID of the token to query.
     * @dev Compute the position using bit-shifting operations to read from [xyz].
     * @return The (x, y, z) position.
     */
    function tokenIdToPosition(uint256 tokenId)
        external
        pure
        returns (uint16 xPosition, uint16 yPosition, uint16 zPosition)
    {
        xPosition = uint16(tokenId >> 32);
        yPosition = uint16(tokenId >> 16);
        zPosition = uint16(tokenId);
    }

    /**
     * Returns the token ID from (x, y, z) position.
     * @param xPosition uint16 X-axis coordinate.
     * @param yPosition uint16 Y-axis coordinate.
     * @param zPosition uint16 Z-axis coordinate.
     * @dev Compute the ID using bit-shifting operations storing [xyz].
     * @return The token ID.
     */
    function tokenPositionToId(
        uint16 xPosition,
        uint16 yPosition,
        uint16 zPosition)
        external
        pure
        returns (uint56 id)
    {
        return (xPosition << 32) | (yPosition << 16) | zPosition;
    }


    // ### Fallback function ###

    /**
     * Fallback function. Accepts everything and log using event.
     */
    function()
        external
        payable
    {
        revert("VoxelToken: No ether accepted directly.");
    }
}